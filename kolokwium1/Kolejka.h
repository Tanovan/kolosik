#pragma once
#include "Atrakcja.h"
class Kolejka : public Atrakcja
{
	int godzina_odjazdu;
	int godzina_przyjazdu;
public:
	Kolejka(void);
	~Kolejka(void);
	int ReturnGodzinaOdjazdu();
	int ReturnGodzinaPrzyjazdu();
	void Inicjuj(int x, int y);
};

