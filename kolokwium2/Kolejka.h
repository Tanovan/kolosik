#pragma once
#include <iostream>
#include<vector>
using namespace std;
class Kolejka
{
	vector <int> kolejka;
public:
	Kolejka(void);
	~Kolejka(void);
	int virtual Pierwszy(vector <int> kolejka)=0;
	void virtual UsunPierwszy(vector <int> kolejka)=0;
	void virtual DodajNaKoniec(vector <int> kolejka, int x)=0;
	bool virtual Pusta(vector <int> kolejka)=0;
	void virtual Exterminate(vector <int> kolejka)=0;
};

