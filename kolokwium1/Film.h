#pragma once
#include "Atrakcja.h"
class Film : public Atrakcja
{
	int czas_trwania;
	string tytul;
public:
	Film(void);
	~Film(void);
	int ReturnCzasTrwania();
	string ReturnTytul();
	void Inicjuj(int x, string y);
};

