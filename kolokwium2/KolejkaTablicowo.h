#pragma once
#include "Kolejka.h"
class KolejkaTablicowo
{
	int ilosc_elementow;
	int tablica[999];
public:
	KolejkaTablicowo(void);
	~KolejkaTablicowo(void);
	int Pierwszy(int tab[]);
	void UsunPierwszy(int tab[], int x);
	void DodajNaKoniec(int tab[], int x, int y);
	bool Pusta(int tab[]);
	void Exterminate(int tab[]);
};

