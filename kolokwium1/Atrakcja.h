#pragma once
#include <iostream>
#include <string>
using namespace std;
class Atrakcja
{
protected:
	int cena;
	string nazwa;
	string opis;
public:
	int ReturnCena();
	string ReturnNazwa();
	string ReturnOpis();
	Atrakcja(void);
	~Atrakcja(void);
};

